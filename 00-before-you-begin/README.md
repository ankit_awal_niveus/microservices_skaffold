# 0. Before you begin

- Install Skaffold: ([binary
  releases](https://github.com/GoogleContainerTools/skaffold/releases))

    ```
    brew install skaffold
    ```
